package exerelin.commandQueue;

// Credit: LazyWizard

public interface BaseCommand
{
    public void executeCommand();
}